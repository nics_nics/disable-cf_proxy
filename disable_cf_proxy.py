#!/usr/bin/python
import requests
import subprocess as sp
import time
import shutil
import os
import signal
import sys
import json
from datetime import date

cfusers=[]

class cfuser:
 def __init__(self,mail,api_key):
  self.mail = mail
  self.api_key = api_key

#add all user accounts
cfusers.append(cfuser('CHANGE_TO_USER_ACCOUNT_EMAIL@EMAIL.COM','CHANGE TO CF API KEY OF THE ACCOUNT'))
#cfusers.append(cfuser('CHANGE_TO_USER_ACCOUNT2_EMAIL@EMAIL.COM','CHANGE TO CF API KEY OF THE ACCOUNT2'))
#cfusers.append(cfuser('CHANGE_TO_USER_ACCOUNT3_EMAIL@EMAIL.COM','CHANGE TO CF API KEY OF THE ACCOUNT3'))

#filenames for settings history keeping
proxy_backup_settings='./disable_cf_proxy-zone_proxy_backup.log'

class zone:
 def __init__(self,name,id):
  self.name = name
  self.id = id
  self.dnsrecords=[]

class parsed_record:
 def __init__(self,dns_name,dns_id,zone_id,user_mail,status):
  self.dns_name = dns_name
  self.dns_id = dns_id
  self.zone_id = zone_id
  self.user_mail = user_mail
  self.status = status

class dns_record:
 def __init__(self,name,id):
  self.dns_name = name
  self.dns_id = id
 def get_x(self):
  return self.__x

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def signal_handler(sig, frame):
 print('\n')
 print('You pressed Ctrl+C! if this was done during the backup file creation the '+bcolors.FAIL+'backup file will be incomplete!!!'+bcolors.ENDC)
 sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
#signal.pause()

def get_accounts(cfuser):
 url='https://api.cloudflare.com/client/v4/accounts?page=1&per_page=50&direction=desc'
 headers = {'X-Auth-Email':cfuser.mail,'X-Auth-Key':cfuser.api_key,'Content-type': 'application/json'}
 r = requests.get(url,headers=headers)
 for account in r.json()['result']:
  print(account)

#get zones
def get_zones(cfuser):
 url = 'https://api.cloudflare.com/client/v4/zones?page=1&per_page=1000&order=status&direction=desc&match=all'
 zones=[]
 headers = {'X-Auth-Email':cfuser.mail,'X-Auth-Key':cfuser.api_key,'Content-type': 'application/json'}
 r = requests.get(url,headers=headers)
 for parsed_zone in r.json()['result']:
  #print(zone['name'],zone['id'])
  zones.append(zone(parsed_zone['name'],parsed_zone['id']))
 return zones

def zone_list_dns_records(cfuser,zone):
 url = 'https://api.cloudflare.com/client/v4/zones/'+zone.id+'/dns_records?proxied=true&page=1&per_page=1000&order=type&direction=desc&match=all'
 headers = {'X-Auth-Email':cfuser.mail,'X-Auth-Key':cfuser.api_key,'Content-type': 'application/json'}
 r = requests.get(url,headers=headers)
 dns_records=[]
 for parsed_record in r.json()['result']:
  dns_records.append(dns_record(parsed_record['name'],parsed_record['id']))
 return dns_records

def backup_records():
 for cfuser in cfusers:
  #user_accounts=[]
  #get_accounts(cfuser)
  zones=get_zones(cfuser)
  f = open(proxy_backup_settings, "w")
  for zone in zones:
   zone.dns_records=zone_list_dns_records(cfuser,zone)
   for record in zone.dns_records:
    #print(record.name,record.id)
    f.write(record.dns_name+"||"+record.dns_id+"||"+zone.id+"||"+cfuser.mail+"||cf_proxy_enabled\n")
  f.close()

def backup_backup_file():
 shutil.copy(proxy_backup_settings,proxy_backup_settings+'.backup-'+date.today().strftime("%d-%m-%Y"))

def check_backup_file():
 if (os.path.exists(proxy_backup_settings)):
  backup_file_created=time.ctime(os.path.getctime(proxy_backup_settings))
  backup_file_modified=time.ctime(os.path.getmtime(proxy_backup_settings))
  print ('DNS Backup file found (created on: '+backup_file_created+' , last modified on: '+backup_file_modified+')')
  return True
 else:
  print('Backup file not found, performing a backup')
  backup_records()
  if (check_backup_file()):
   return True
  else:
   return False

def parse_records():
 f = open(proxy_backup_settings, "r")
 lines=f.readlines()
 parsed_records=[]
 for line in lines:
  split_line=line.split('||')
  #print(split_line)
  dns_name=split_line[0]
  dns_id=split_line[1]
  zone_id=split_line[2]
  user_mail=split_line[3]
  cf_proxy=split_line[4]
  parsed_records.append(parsed_record(dns_name,dns_id,zone_id,user_mail,cf_proxy))
 f.close()
 return parsed_records

def cf_disable_proxy(record):
 url = 'https://api.cloudflare.com/client/v4/zones/'+record.zone_id+'/dns_records/'+record.dns_id
 cfuser=[user for user in cfusers if user.mail == record.user_mail][0]
 #print(cfuser.mail)
 #print(cfuser.api_key)
 headers = {'X-Auth-Email':cfuser.mail,'X-Auth-Key':cfuser.api_key,'Content-type': 'application/json'}
 proxy_set={'proxied':False}
 r = requests.patch(url,data=json.dumps(proxy_set),headers=headers)
 #print(url)
 #print(r.content)
 print(record.dns_name+bcolors.FAIL+' proxy disabled'+bcolors.ENDC)

def cf_enable_proxy(record):
 url = 'https://api.cloudflare.com/client/v4/zones/'+record.zone_id+'/dns_records/'+record.dns_id
 cfuser=[user for user in cfusers if user.mail == record.user_mail][0]
 #print(cfuser.mail)
 #print(cfuser.api_key)
 headers = {'X-Auth-Email':cfuser.mail,'X-Auth-Key':cfuser.api_key,'Content-type': 'application/json'}
 proxy_set={'proxied':True}
 r = requests.patch(url,data=json.dumps(proxy_set),headers=headers)
 print(record.dns_name+bcolors.OKGREEN+' proxy enabled'+bcolors.ENDC)

def modify_cf_proxy_of_records(disable):
 parsed_records=parse_records()
 for record in parsed_records:
  #print('parsed:'+record.zone_name+record.dns_id+record.zone_id+record.user_mail+record.status)
  if (disable):
   cf_disable_proxy(record)
  else:
   cf_enable_proxy(record)

backup_exists=check_backup_file()
if (backup_exists):
 print('Please enter '+bcolors.OKGREEN+'disable,backup'+bcolors.ENDC+' or '+bcolors.OKGREEN+'restore'+bcolors.ENDC+' to perform new operation: ')
 choice=raw_input()
 if (choice=='backup'):
  backup_backup_file()
  backup_records()
 elif (choice=='disable'):
  modify_cf_proxy_of_records(disable=True)
  print(bcolors.OKGREEN+'completed'+bcolors.ENDC)
  exit()
 elif (choice=='restore'):
  modify_cf_proxy_of_records(disable=False)
  print(bcolors.OKGREEN+'completed'+bcolors.ENDC)
  exit()
 else:
  print('no operation chosen, skip')
  exit()
else:
 print(bcolors.FAIL+'FATAL! backup file does not exist and could not be created!'+bcolors.ENDC)

