# README #  
  
This is a python (2.7.5) script to disable CF_PROXY for all DNS records accessible from the included accounts.  
  
### Customization  
  
* Configuration
  Add all accounts in disable_cf_proxy.py in the following form:
  cfusers.append(cfuser('account_email','account_cloudflare_api_key'))
* Usage
  Run disable_cf_proxy.py.
  -The script performs an initial backup of all accessible records and keeps them in the file disable_cf_proxy-zone_proxy_backup.log  
  -After the initial run check all dns records are included in the file  
  -Run again and type/enter the operation:  
   -backup create a new log file  
   -disable disables CF_PROXY for all enabled DNS records  
   -restore enables CF_PROXY for all DNS records in the logfile  

### TODO:  
* Add additional checks in order to paginate the results (based on CF API results per page limit)  
